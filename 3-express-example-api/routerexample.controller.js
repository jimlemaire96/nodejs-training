import { Router } from 'express';

const router = Router();

router.route('/router/specific-route')
        .get((req,res,next) => {
            res.send('You\'re inside a child router of your express application now!');
        })
        

export default router;