import Express from 'express';
import router from './routerexample.controller.js';

const hostname = '127.0.0.1';
const port = 3001;

const app = Express();



app.get('/', (req,res,next) => {
    res.send('Hello world!')
})

app.get('/specific-route', (req,res,next) => {
    res.send('Hello world but from specific-route!')
})

// serving of static resources
app.use('/serve-static', Express.static('../0-data'));

//mounting the router on the main express app, this will instanciate /router/specific-route endpoint
app.use(router);

app.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});