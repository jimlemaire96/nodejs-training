/** intercepting unallowed http METHOD (GET/POST/PUT/DELETE)
 * 
 * @param {Express.Request} req 
 * @param {Express.Response} res 
 * @param {Express.next} next 
 */
export const methodNotAllowedMiddleware = (req,res) => {
    console.warn(`unallowed ${req.method} call over ${req.path}`);
    res.status(405).send('Method not allowed');
    
}

