/** incoming request logging middleware
 * 
 * @param {Express.Request} req 
 * @param {Express.Response} res 
 * @param {Express.next} next 
 */
export const loggingMiddleware = (req,res,next) => {
    console.log(`Incoming ${req.method} request over ${req.path}`)
    // calling next to force-call next middleware in the stack
    next();
}

