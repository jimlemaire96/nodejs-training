/** error handling middleware
 *  
 * @param {Error} err 
 * @param {Express.Request} req 
 * @param {Express.Response} res 
 * @param {Express.next} next 
 */
export const errorHandlingMiddleware = (err, req, res, next) => {
    // perform some error logging server-side to have logs about the error thrown.
    console.error(`
        An error has been thrown by a ${req.method} call on path ${req.path}.
        error: ${err}
    `)
    // return a 500 response to the request emitter to shut off the current request.
    res.status(500).send('Something wrong happened')
} 

