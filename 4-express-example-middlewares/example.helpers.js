const aBogusFunction = () => {
    return new Promise((resolve, reject) => {
        reject('A mysterious error appeared !')
    })
}

export {
    aBogusFunction
}