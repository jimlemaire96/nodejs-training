import Express from 'express';
import {
    loggingMiddleware
} from './middlewares/logging.middleware.js';
import {
    errorHandlingMiddleware
} from './middlewares/error.middleware.js';
import {
    methodNotAllowedMiddleware
} from './middlewares/httpMethod.middleware.js';
import {
    aBogusFunction
} from './example.helpers.js';

const hostname = '127.0.0.1';
const port = 3001;

const app = Express();

//Applying my logging middleware to the entire application context.
// this means that any incoming request will pass by this logging middleware.
app.use(loggingMiddleware);

// simple GET Hello World endpoint
app.get('/', (req, res, next) => {
    res.send('Hello world!')
})

/**
 * 
 * reusable Error handling middleware example
 * 
 */
app.route('/example/error')
    .get(async (req, res, next) => {
        try {
            await aBogusFunction()
        } catch (err) {
            // ... you could perform some more logging here if you see fit

            // if an error gets eventually thrown, 
            //  calling next() with the error as its parameter will automatically delegate it 
            //  to the first error handling middleware in the matching route middlewares stack
            next(err);
        }
    })
    // adding our error handling express middleware to this route.
    .all(errorHandlingMiddleware)

/**
 * 
 * method differenciation
 * 
 */
app.route('/example/methods')
    .get((req, res) => {
        res.send(`I'm the GET result for the route /example/methods`);
    })
    .post((req, res) => {
        res.send(`I'm the POST result for the route /example/methods`);
    });

/**
 * 
 * Process post data
 * 
 */
app.route('/example/methods/postdata')
    //This is the exported middleware from express to state that you expect him to 
    //  populate req.body with any json data that might be posted within this route.
    .all(Express.json())
    .post((req, res) => {
        if (!req.body || req.body === {}) {
            res.status(400).send('This call expects a payload.')
        }
        console.log(req.body)
        res.send(`You were looking for ${req.body.firstname} ${req.body.lastname}`)
    });


/**
 * 
 * URL parameters and query parameters
 * 
 */
app.route('/example/urlparameter/:param1/:param2')
    .get((req, res) => {
        // you can access the request parameters with the .params member on the request object.
        console.log(`${req.method} ${req.path} with URL parameters :\n${req.params}`)
        if (req.query) {
            // you can access the request query parameters
            console.log(`${req.method} ${req.path} with URL query :\n${req.query}`)
        }
    })
    .post((req, res) => {
        res.send(`I'm the POST result for the route /example/methods`);
    });




/**
 * 
 * reusable "Method not allowed"(405) handler middleware example
 * 
 */
app.route('/example/methodnotallowed')
    .get((req, res) => {
        res.send(`${req.method}  is allowed and I'm processing it`);
    })
    // adding our error handling express middleware to this route.
    .all(methodNotAllowedMiddleware)

app.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
});