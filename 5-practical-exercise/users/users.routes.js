import {json, Router} from 'express';
import {findAll, findOne, addOne, deleteOne, updateOne} from './users.service.js';

const router = Router();
router.route('/users')
    .get(async (req, res, next) => {
        res.send(await findAll());
    })
    .post(json(), async (req, res, next) => {
        try {
            if (req.body.data) {
                const {email, username, profile, roles} = req.body.data;
                const user = await addOne(email, username, profile, roles)
                res.send(user._id);
            } else {
                res.status(400).send('Expected payload missing.')
            }
        } catch (err) {
            next(err);
        }
    })

router.route('/users/:id')
    .get(async (req, res, next) => {
        const {id} = req.params;
        if (!id) res.status(400);
        res.send(await findOne(id));
    })
    .delete(async (req, res, next) => {
        const {id} = req.params;
        if (!id) res.status(400);
        const deleteResult = await deleteOne(id);
        deleteResult.deletedCount === 1 ? res.sendStatus(200) : res.sendStatus(400);
    })
    .put(
        json(),
        async (req, res, next) => {
            const {id} = req.params;
            if (!id) res.status(400);
            if (req.body.data) {
                const update = await updateOne(id, req.body.data);
                res.send(update);
            }
        }
    )


export default router;
