import UsersModel from "./users.model.js";

const findAll = async () => UsersModel.find();

const findOne = async (id) => UsersModel.find({ _id: id});

const addOne = async (email, username, profile, roles) => {
    const _user = {
        email,
        username,
        profile,
        roles,
    }
    return new UsersModel(_user).save();
}

const deleteOne = async (id) => {
    return UsersModel.deleteOne({ _id : id})
}

const updateOne = async (id, update) => {
    return UsersModel.findByIdAndUpdate(id, update)
}

export {
    addOne,
    findAll,
    findOne,
    deleteOne,
    updateOne
}

