import mongoose from 'mongoose';

const {Schema} = mongoose;

const userSchema = new Schema({
    email: {type: String, required: true, unique: true},
    username: {type: String, required: false, unique: true},
    profile: {
        name: String,
        company: String,
    },
    roles: [{
        type: String,
        default: 'guest',
        enum: ['guest', 'member', 'admin']
    }],
    createdAt: {type: Date, default: Date.now},
    updatedAt: {type: Date, default: Date.now}
});

export default mongoose.model('Users', userSchema);
