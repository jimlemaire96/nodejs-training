import Express from 'express';
import userRouter from "./users/users.routes.js";
import authRouter from "./auth/auth.routes.js";
import getPassport from "./passport.js";
import session from "express-session";


const app = Express();

const API_PREFIX = '/api';

app.use(getPassport().initialize());
app.use(session({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: false,
    cookie: {secure: true}
}));
app.use(getPassport().session())
app.use(API_PREFIX, userRouter)
app.use(API_PREFIX, authRouter)

export default app;
