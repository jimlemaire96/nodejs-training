import {Router, json} from 'express';
import getPassport from '../passport.js';

const router = Router();

router.post('/auth/login',
    json(),
    getPassport().authenticate('local', { failureRedirect: '/login' }),
    function(req, res) {
        res.redirect('/');
    });

export default router;
