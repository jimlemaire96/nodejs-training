import * as dotenv from 'dotenv';

dotenv.config();
import app from './app.js';
import {connect} from "mongoose";
import session from 'express-session';
import getPassport from "./passport.js";

try {
    await connect('mongodb://127.0.0.1:27017/nodejs-training');

    app.listen(process.env.SERVER_PORT, process.env.SERVER_HOSTNAME, () => {
            console.log(`Example app listening with hostname ${process.env.SERVER_HOSTNAME} on port ${process.env.SERVER_PORT}`)
        }
    );

} catch (e) {
    console.error('something wrong happened while booting', e);
    process.exit(1);
}
