function updateAllProps(object, update){
    Object.keys(object).map(key => {
        if(object[key] && update[key] && object[key] !== update[key]){
            object[key] = update[key];
        }
    })
    return object;
}

export {
    updateAllProps
}
