import passport from 'passport';

import {Strategy as LocalStrategy} from 'passport-local';
import UsersModel from "./users/users.model.js";

let myPassport = null;

function preparePassportInstance() {
    console.log('prepping passport');
    myPassport = new passport.Passport();

    console.log('adding local strategy passport');
    myPassport.use(new LocalStrategy(
            function (username, password, done) {

                console.log('matching user with passport');
                console.log(username);
                console.log(password);
                UsersModel.findOne({email: username})
                    .then(function (user) {
                        if (!user) {
                            return done(null, false);
                        }
                        if (password !== 'pwd') {
                            return done(null, false);
                        }
                        console.log(user);
                        return done(null, user);
                    })
                    .catch(function (err) {
                        console.log(err);
                        return done(err);
                    });
            }
        )
    )
}

function getPassport() {
    if (myPassport === null)
        preparePassportInstance();

    return myPassport;
}

export default getPassport;
